# TODO #

This is a simple todo app in Meteor.js

Written as a learning tool.

User can add todo tasks, mark them completed or prioritized.
Each task can have subtasks with the same functionality.