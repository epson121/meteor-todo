  // counter starts at 0
  Session.setDefault('counter', 0);
  Session.set('should_show_subtask_add_form', "");
  Session.set("selectedItem", false);

  Template.registerHelper("toggleSession", function (session_variable) {
      if(Session.get(session_variable)){
         Session.set(session_variable, false);
      } else{
         Session.set(session_variable, true);
      }
  });

  Template.hello.helpers({
    counter: function () {
      return Session.get('counter');
    }
  });

  Template.hello.events({
    'click button': function () {
      // increment the counter when button is clicked
      Session.set('counter', Session.get('counter') + 3);
    },
  });

  Template.todos.helpers({
    title: function() {
      return "This is a title";
    },

    todos: function() {
      return Todos.find({}).fetch();
    },

    isPriority: function(item) {
      return item.priority ? "priority" : "";
    },

    isFinished: function(item) {
      return item.done;
    },

    isCheckedPriority: function(item) {
      return item.priority ? "" : false;
    },

    isCheckedFinished: function(item) {
      return item.done ? "" : false;
    },

    showSubtaskForm: function(id) {
      return Session.get('should_show_subtask_add_form') == id;
    },

    subtasks: function(id) {
      return Subtasks.find({parent: id}).fetch();
    }

  });

  Template.todos.events({
    'click .finish': function(event) {
      var element = $(event.target);
      var isChecked = element.is(":checked");
      var id = element.attr("id");
      Todos.update({_id: id}, {$set: {done: isChecked}});
      Subtasks.update({_id: id}, {$set: {done: isChecked}});
    },

    'click .prioritize': function(event) {
      var element = $(event.target);
      var isChecked = element.is(":checked");
      var id = element.attr("id");
      Todos.update({_id: id}, {$set: {priority: isChecked}});
      Subtasks.update({_id: id}, {$set: {priority: isChecked}});
    },

    'click .delete': function(event) {
      var element = $(event.target);
      var id = element.attr("id");
      Todos.remove({_id: id});
      Subtasks.remove({_id: id});
      Subtasks.remove({parent: id});
    },

    'click #show_form': function(event) {
      if(Session.get('should_show_todo_add_form')){
        Session.set('should_show_todo_add_form', false);
      } else{
        Session.set('should_show_todo_add_form', true);
      }
    },

    'click .add_subtask': function(event) {
      if(Session.get('should_show_subtask_add_form')){
        Session.set('should_show_subtask_add_form', "");
      } else{
        Session.set('should_show_subtask_add_form', event.target.id);
      }
      Session.set('selectedItem', event.target.id);
    },
  });

  Template.todo_add.helpers({
    showForm: function() {
      return Session.get('should_show_todo_add_form');
    }
  });

  Template.subtask_add.helpers({
    showForm: function() {
      return Session.get('should_show_form');
    }
  });


  Template.todo_add.events({
    'submit #todo_add': function(event) {
      event.preventDefault();
      var element = event.target;
      var body = element.body.value;
      var priority = element.priority.checked;
      if (!body)
        return;
      Todos.insert({
        body: body,
        priority: priority
      });
      element.body.value = "";
      element.priority.checked = false;
    }
  });

  Template.subtask_add.events({
    'submit #subtask_add': function(event) {
      event.preventDefault();
      var element = event.target;
      var body = element.body.value;
      var priority = element.priority.checked;
      var itemId = Session.get('selectedItem');
      if (!body)
        return;
      Subtasks.insert({
        body: body,
        priority: priority,
        parent: itemId
      });
      element.body.value = "";
      element.priority.checked = false;
    }
  });